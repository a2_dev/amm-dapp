import { HardhatRuntimeEnvironment } from 'hardhat/types';
import { DeployFunction } from 'hardhat-deploy/types';
import { ethers, upgrades } from 'hardhat';
import {
  MasterChefFactory,
  LettyFactory,
  Letty,
} from '../typechain';

const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
    /*
  ░██╗░░░░░░░██╗░█████╗░██████╗░███╗░░██╗██╗███╗░░██╗░██████╗░
  ░██║░░██╗░░██║██╔══██╗██╔══██╗████╗░██║██║████╗░██║██╔════╝░
  ░╚██╗████╗██╔╝███████║██████╔╝██╔██╗██║██║██╔██╗██║██║░░██╗░
  ░░████╔═████║░██╔══██║██╔══██╗██║╚████║██║██║╚████║██║░░╚██╗
  ░░╚██╔╝░╚██╔╝░██║░░██║██║░░██║██║░╚███║██║██║░╚███║╚██████╔╝
  ░░░╚═╝░░░╚═╝░░╚═╝░░╚═╝╚═╝░░╚═╝╚═╝░░╚══╝╚═╝╚═╝░░╚══╝░╚═════╝░
  Check all variables below before execute the deployment script
  */

  const TOKEN_ADDR = '0x472620F8D26d20A50B2f35180A3f9bc6cEcBB7E9'; // token
  const TOKENS_PER_BLOCK = ethers.utils.parseEther('20'); // tokens per block available
  const START_BLOCK = '7328000'; // bonus tokens for starting the block
  const BONUS_LOCKUP = '7000'; // bonus tokens for lockup
  const BONUS_END_BLOCK = '7650000'; // bonus tokens for end of complete block









  const { deployments, getNamedAccounts, network } = hre;
  const { deployer } = await getNamedAccounts();

  console.log(`>> Deploying an upgradable MasterChef contract`);
  const tokenItem = LettyFactory.connect(
    TOKEN_ADDR, (await ethers.getSigners())[0]
  ) as Letty;

  const MasterChef = (await ethers.getContractFactory(
    'MasterChef',
    (await ethers.getSigners())[0]
  )) as MasterChefFactory;
  const masterChef = await upgrades.deployProxy(MasterChef, [tokenItem.address, deployer, TOKENS_PER_BLOCK, START_BLOCK, BONUS_LOCKUP, BONUS_END_BLOCK]);
  await masterChef.deployed();

  console.log(`✔ MasterChef done:`);
  console.log(`✔ Deployed at ${masterChef.address}`);

  console.log(">> Transferring ownership of Token from deployer to the Vault");
  await tokenItem.transferOwnership(masterChef.address, { gasLimit: '500000' });
  console.log("✔ Done");
};

export default func;
func.tags = ['MasterChef'];