import { HardhatRuntimeEnvironment } from 'hardhat/types';
import { DeployFunction } from 'hardhat-deploy/types';
import { ethers, upgrades } from 'hardhat';
import {
  VaultFactory,
} from '../typechain';

const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
    /*
  ░██╗░░░░░░░██╗░█████╗░██████╗░███╗░░██╗██╗███╗░░██╗░██████╗░
  ░██║░░██╗░░██║██╔══██╗██╔══██╗████╗░██║██║████╗░██║██╔════╝░
  ░╚██╗████╗██╔╝███████║██████╔╝██╔██╗██║██║██╔██╗██║██║░░██╗░
  ░░████╔═████║░██╔══██║██╔══██╗██║╚████║██║██║╚████║██║░░╚██╗
  ░░╚██╔╝░╚██╔╝░██║░░██║██║░░██║██║░╚███║██║██║░╚███║╚██████╔╝
  ░░░╚═╝░░░╚═╝░░╚═╝░░╚═╝╚═╝░░╚═╝╚═╝░░╚══╝╚═╝╚═╝░░╚══╝░╚═════╝░
  Check all variables below before execute the deployment script
  */

  const TOKEN_ADDR = '0x472620F8D26d20A50B2f35180A3f9bc6cEcBB7E9'; // token address
  const CONTROLLER_ADDR = '0x6400d53adbF6f043aCdD6EE324610239A151708D'; // controller address

  const CONTROLLER_NAME = "Letty Controller System"









  console.log(`>> Deploying an upgradable Vault contract for ${CONTROLLER_NAME}`);
  const Vault = (await ethers.getContractFactory(
    'Vault',
    (await ethers.getSigners())[0]
  )) as VaultFactory;
  const vault = await upgrades.deployProxy(
    Vault, [ TOKEN_ADDR, CONTROLLER_ADDR ]
  );
  await vault.deployed();
  console.log(`✔ Vault done:`);
  console.log(`✔ Deployed at ${vault.address}`);
};

export default func;
func.tags = ['Vault'];