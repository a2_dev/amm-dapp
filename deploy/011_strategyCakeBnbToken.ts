import { HardhatRuntimeEnvironment } from 'hardhat/types';
import { DeployFunction } from 'hardhat-deploy/types';
import { ethers, upgrades } from 'hardhat';
import {
  StrategyCakeBnbTokenFactory,
} from '../typechain';

const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
    /*
  ░██╗░░░░░░░██╗░█████╗░██████╗░███╗░░██╗██╗███╗░░██╗░██████╗░
  ░██║░░██╗░░██║██╔══██╗██╔══██╗████╗░██║██║████╗░██║██╔════╝░
  ░╚██╗████╗██╔╝███████║██████╔╝██╔██╗██║██║██╔██╗██║██║░░██╗░
  ░░████╔═████║░██╔══██║██╔══██╗██║╚████║██║██║╚████║██║░░╚██╗
  ░░╚██╔╝░╚██╔╝░██║░░██║██║░░██║██║░╚███║██║██║░╚███║╚██████╔╝
  ░░░╚═╝░░░╚═╝░░╚═╝░░╚═╝╚═╝░░╚═╝╚═╝░░╚══╝╚═╝╚═╝░░╚══╝░╚═════╝░
  Check all variables below before execute the deployment script
  */

  // ----------------!!!---------------------
  // SOME ADDRESSES ARE PLACED INSIDE SOL FILE
  // ----------------!!!---------------------
  const CONTROLLER_ADDR = '0x6400d53adbF6f043aCdD6EE324610239A151708D';
  const TOKEN_WANT_ADDR = '0x472620F8D26d20A50B2f35180A3f9bc6cEcBB7E9'; // token address
  const FARM_LP_ADDRESS = '0x0cfc7D6FFF1B3a9EA126203c9a6Ee8956A404099'; // farm address (token)
  const B_TOKEN_ADDRESS = '0x2b8ff854c5e16cf35b9a792390cc3a2a60ec9ba2'; // other toekn to change to (address). For example BNB









  console.log(">> Deploying an upgradable StrategyCakeBnbToken contract");
  const StrategyCakeBnbToken = (await ethers.getContractFactory(
    "StrategyCakeBnbToken",
    (await ethers.getSigners())[0],
  )) as StrategyCakeBnbTokenFactory;
  const strategyCakeBnbToken = await upgrades.deployProxy(StrategyCakeBnbToken, [CONTROLLER_ADDR, TOKEN_WANT_ADDR, B_TOKEN_ADDRESS, FARM_LP_ADDRESS]);
  await strategyCakeBnbToken.deployed();
  console.log(`✔ Deployed at ${strategyCakeBnbToken.address}`);
};

export default func;
func.tags = ['StrategyCakeBnbToken'];