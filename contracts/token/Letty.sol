// SPDX-License-Identifier: GPL-3.0-only
pragma solidity <0.9.0;

import '@openzeppelin/contracts/utils/math/SafeMath.sol';
import '@openzeppelin/contracts/access/Ownable.sol';
import '@openzeppelin/contracts/token/ERC20/ERC20.sol';

contract Letty is ERC20("Letty", "LTY"), Ownable {
  using SafeMath for uint256;

  uint256 private constant CAP = 200_000_000e18;
  uint256 private _totalLock;

  uint256 public startReleaseBlock;
  uint256 public endReleaseBlock;
  uint256 public constant MANUAL_MINT_LIMIT = 7_000_000e18;
  uint256 public manualMinted;

  mapping(address => uint256) private _locks;
  mapping(address => uint256) private _lastUnlockBlock;

  event Lock(address indexed to, uint256 value);

  constructor(uint256 _startReleaseBlock, uint256 _endReleaseBlock) {
    manualMinted = 0;

    require(_endReleaseBlock > _startReleaseBlock, 'endReleaseBlock < startReleaseBlock');
    startReleaseBlock = _startReleaseBlock;
    endReleaseBlock = _endReleaseBlock;
    manualMint(msg.sender, 250000e18);
  }

  function cap() public pure returns (uint256) {
    return CAP;
  }

  function manualMint(address _to, uint256 _amount) public onlyOwner {
    require(manualMinted <= MANUAL_MINT_LIMIT, 'mint limit exceeded');
    manualMinted = manualMinted.add(_amount);
    mint(_to, _amount);
  }

  function mint(address _to, uint256 _amount) public onlyOwner {
    require(totalSupply().add(_amount) <= cap(), 'cap exceeded');
    require(_to != address(0), 'ERC20: mint to the zero address');
    _mint(_to, _amount);
  }

  function burn(address _account, uint256 _amount) public onlyOwner {
    require(_account != address(0), 'ERC20: burn to the zero address');
    _burn(_account, _amount);
  }

  function transferStackedTokens(address _account, uint256 _amount) public onlyOwner {
    require(_account != address(0), 'ERC20: mint to the zero address');
    _transfer(address(this), _account, _amount);
  }

  function unlockedSupply() external view returns (uint256) {
    return totalSupply().sub(totalLock());
  }

  function totalLock() public view returns (uint256) {
    return _totalLock;
  }

  function lockOf(address _account) external view returns (uint256) {
    return _locks[_account];
  }

  function lastUnlockBlock(address _account) external view returns (uint256) {
    return _lastUnlockBlock[_account];
  }

  function lock(address _account, uint256 _amount) external onlyOwner {
    require(_account != address(0), 'cannot lock to address(0)');
    require(_amount <= balanceOf(_account), 'cannot lock over existing balance');

    _transfer(_account, address(this), _amount);

    _locks[_account] = _locks[_account].add(_amount);
    _totalLock = _totalLock.add(_amount);

    if (_lastUnlockBlock[_account] < startReleaseBlock) {
      _lastUnlockBlock[_account] = startReleaseBlock;
    }

    emit Lock(_account, _amount);
  }

  function canUnlockAmount(address _account) public view returns (uint256) {
    // When block number less than startReleaseBlock, no tokens can be unlocked
    if (block.number < startReleaseBlock) {
      return 0;
    }
    // When block number more than endReleaseBlock, all locked tokens can be unlocked
    else if (block.number >= endReleaseBlock) {
      return _locks[_account];
    }
    // When block number is more than startReleaseBlock but less than endReleaseBlock,
    // some tokens can be released
    else {
      uint256 releasedBlock = block.number.sub(_lastUnlockBlock[_account]);
      uint256 blockLeft = endReleaseBlock.sub(_lastUnlockBlock[_account]);
      return _locks[_account].mul(releasedBlock).div(blockLeft);
    }
  }

  function unlock() external {
    require(_locks[msg.sender] > 0, 'no locked tokens exist');

    uint256 amount = canUnlockAmount(msg.sender);

    _transfer(address(this), msg.sender, amount);
    _locks[msg.sender] = _locks[msg.sender].sub(amount);
    _lastUnlockBlock[msg.sender] = block.number;
    _totalLock = _totalLock.sub(amount);
  }

  // @dev move tokens with its locked funds to another account
  function transferAll(address _to) external {
    _locks[_to] = _locks[_to].add(_locks[msg.sender]);

    if (_lastUnlockBlock[_to] < startReleaseBlock) {
      _lastUnlockBlock[_to] = startReleaseBlock;
    }

    if (_lastUnlockBlock[_to] < _lastUnlockBlock[msg.sender]) {
      _lastUnlockBlock[_to] = _lastUnlockBlock[msg.sender];
    }

    _locks[msg.sender] = 0;
    _lastUnlockBlock[msg.sender] = 0;

    _transfer(msg.sender, _to, balanceOf(msg.sender));
  }
}
